# Inclus le fichier .env permettant de définir des préférences générales
include .env
export

# Définit le shell par défaut pour les différents systèmes
SHELL = /bin/sh

# Récupère le numéro d'utilisateur système pour l'utilisateur courant : ex : 1000 pour linux, 501 pour macos
CURRENT_UID := $(shell id -u)
export CURRENT_UID

# Fonctions ayant besoins de paramètres, permet de les gérer, de les formater et de les utiliser
SUPPORTED_COMMANDS := newSF newWP newPHP remove dump rename dumpInsert newClone verifNomProjet
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  NOM := $(wordlist 2,2,$(MAKECMDGOALS))
  NOM2 := $(wordlist 3,3,$(MAKECMDGOALS))
  $(eval $(NOM):;@:)
  NOM2 := $(subst :,\:,$(NOM2))
  $(eval $(NOM2):;@:)
  NOMOK := $(shell expr $(NOM) : "^[a-z][a-z0-9]*$$")
endif

# Permet de définir la comande de docker-compose avec le bon utilisateur et la bonne version de php
DK := USERID=$(CURRENT_UID) PHPVersion=$(PHP) $(DKC)

# Récupère la date
CURRENT_TIME := $(shell date "+%Y%m%d%H%M")

# .Phony permet de considérer les entrées suivantes comme des commandes et non des fichiers
.PHONY: rename build verifNomProjet preNew postNew newSF newPHP newWP up down cleanAll help removeSF removePHP removeWP check_clean bash dump list updatePhp dumpInsert newClone env restart

# Si on tape just make, cela revient à make help
.DEFAULT_GOAL := help

env: ## voir les variables d'environnements actives
	@echo "- Id utilisateur: $(CURRENT_UID)"
	@echo "- Version PHP: $(PHP)"
ifeq ($(SF),)
	@echo "- Dernière version de Symfony"
else
	@echo "- Version Symfony: $(SF)"
endif

build: ## Construit les conteneurs comme docker-compose build
	@$(DK) build

list: ## Liste tous les projets existants
	@echo "---Projets Symfony---"
	@grep -rnw --include=\*.conf 'virtualhosts' -e 'Symfony' | cut -d/ -f2 | cut -d. -f1
	@echo "---Projets Wordpress---"
	@grep -rnw --include=\*.conf 'virtualhosts' -e 'Wordpress' | cut -d/ -f2 | cut -d. -f1
	@echo "---Projets Php---"
	@grep -rnw --include=\*.conf 'virtualhosts' -e 'PHP' | cut -d/ -f2 | cut -d. -f1

rename: ## Renomme un projet (Symfony, WP, Php) et sa BD : make rename ancien_nom nouveau_nom
ifneq ($(and $(NOM),$(NOM2)),)
	@make up
	@echo "Renommage de la base de données"
	@$(DK) exec db mysqldump -u$(MYSQL_USER) -p$(MYSQL_PASSWORD) -R $(NOM) > /tmp/$(NOM)-dump.sql
	@$(DK) exec db mysqladmin -u$(MYSQL_USER) -p$(MYSQL_PASSWORD) create $(NOM2)
	@cat /tmp/$(NOM)-dump.sql | $(DK) exec -T db mysql -u$(MYSQL_USER) -p$(MYSQL_PASSWORD) $(NOM2)
	@$(DK) exec db mysqladmin -u$(MYSQL_USER) -p$(MYSQL_PASSWORD) drop $(NOM)
	@echo "Renommage du dossier du projet"
	@mv $(APP_PATH)/$(NOM) $(APP_PATH)/$(NOM2)
	@echo "Renommage du virtualhost"
	@mv virtualhosts/$(NOM).conf virtualhosts/$(NOM2).conf
	@sed -i 's/$(NOM)/$(NOM2)/' virtualhosts/$(NOM2).conf
	@echo "Pensez à modifier l'accès à la base de données dans votre code pour $(NOM2)"
else 
	@echo "il faut ajouter l'ancien nom du projet et le nouveau nom du projet à la commande"
endif

restart: down up ## Redémarre les serveurs (down puis up)

verifNomProjet:
ifeq ($(NOMOK),0)
	@echo "Le nom du projet ne doit contenir que des lettres minuscules et des chiffres : (pas de _ , - , ...)"
ERREUR := "true"
endif


# code commun à plusieurs commandes, n'est pas utilisé directement
preNew:
	@make up
	@sleep 5
	@echo "création de la base de donnée"
	@$(DK) exec db mysql -u$(MYSQL_USER) -p$(MYSQL_PASSWORD) -e "CREATE DATABASE $(NOM)"

postNew:
	@make up
	@printf "Pour aller voir votre site :\t\t\033[1m\e[92mhttp://%b.localhost:8000\033[m\e[0m\tEnjoy !!!\n" $(NOM)

# je n'ai pas réussi à le faire fonctionner comme je le voulais, c'est ç dire quelque soit le projet cloner. Il faudrait faire un newCloneSF, newCloneWP, ...
newClone: ##  (non fonctionnel !!!) Crée un projet à partir d'un clone git : make newClone mon_projet https://gitlab.univ-lr.fr/...
ifneq ($(and $(NOM),$(NOM2)),)
# vérification que le dépôt existe..
	@echo "vérification que le dépôt existe"
#	@make preNew
	@echo "création de la BD"
	@echo "création du projet $(NOM)"
#	@git clone $(NOM2) projets/$(NOM)
	@echo "git clone"
# si le dépôt contient le fichier symfony.lock --> Symfony, s'il contient le fichier wp-cli.yml --> wordpress sinon php standard
#ifeq (,$(wildcard projets/$(NOM)/symfony.lock))
	@echo "test si le projet est symfony, wordpress ou php pour créer le bon virtualhost"
	@echo "Pensez à bien renseigner la BD"
	@echo "Enjoy !!!"
else 
	@echo "il faut ajouter le nom du projet et l'adresse du dépôt git"
endif

# new : on crée la BD puis on crée le projet et on configure un nouveau virtualhost à partir d'un modèle

newSF: verifNomProjet
newPHP: verifNomProjet
newWP: verifNomProjet

newSF: ## Crée un nouveau projet Symfony : make newSF mon_projet_SF
ifdef NOM
ifndef ERREUR
	@make preNew
	@echo "création du projet symfony $(NOM)"
	@$(DK) exec php composer create-project symfony/skeleton$(SF) $(NOM)
	@echo "configuration de la base de données via .env.local"
	@echo "DATABASE_URL=mysql://root:root@db:3306/$(NOM)?serverVersion=10.6.7-MariaDB" > $(APP_PATH)/$(NOM)/.env.local
	@echo "MAILER_DSN=smtp://mailer:1025" >> $(APP_PATH)/$(NOM)/.env.local
	@make down
	@echo "création du virtualhost"
	@sed -E 's/xxxxxx/$(NOM)/' ./virtualhosts/symfony.conf.sample >  ./virtualhosts/$(NOM).conf
	@make postNew
endif
else 
	@echo "il faut ajouter le nom du projet à la commande"
endif

newPHP: ## Crée un nouveau projet PHP : make newPHP mon_projet_PHP
ifdef NOM
ifndef ERREUR
	@make preNew
	@echo "création du projet php $(NOM)"
	@mkdir $(APP_PATH)/$(NOM)
	@touch $(APP_PATH)/$(NOM)/index.php
	@make down
	@echo "création du virtualhost"
	@sed -E 's/xxxxxx/$(NOM)/' ./virtualhosts/php.conf.sample >  ./virtualhosts/$(NOM).conf
	@make postNew
endif
else 
	@echo "il faut ajouter le nom du projet à la commande"
endif


newWP: ## Crée un nouveau projet Wordpress : make newWP mon_projet_WP
ifdef NOM
ifndef ERREUR
	@make preNew
	@echo "création du projet wordpress $(NOM)"
	@$(DK) exec php composer create-project roots/bedrock $(NOM)
	@echo "création du virtualhost"
	@sed -E 's/xxxxxx/$(NOM)/' ./virtualhosts/wordpress.conf.sample >  ./virtualhosts/$(NOM).conf
	@echo "modification du .env"
	@$(DK) exec php cp $(NOM)/.env.example $(NOM)/.env
	@$(DK) exec php sed -i '1,3 s/^/#/' $(NOM)/.env
	@$(DK) exec php sed -i '14 s/^/#/' $(NOM)/.env
	@$(DK) exec php sed -i -e "8iDATABASE_URL=mysql://root:root@db:3306/$(NOM)"  $(NOM)/.env
	@$(DK) exec php sed -i -e "14iWP_HOME=http://$(NOM).localhost:8000"  $(NOM)/.env
	@make down
	@make postNew
endif
else 
	@echo "il faut ajouter le nom du projet à la commande"
endif

up: ## Démarre les serveurs
	@$(DK) up -d

down: ## Arrête les serveurs
	@$(DK) down

cleanAll: check_clean ## Donne les commandes pour tout supprimer
	@echo "Attention, action irréversible !!!"
	@echo "Faites make down puis lancer la commande suivante éventuellement avec sudo"
	@echo "docker system prune --volumes -a"

# on suppirme la base de données, le dossier et le virtualhost
remove: ## Supprime un projet PHP, Symfony ou Wordpress : make remove nom_du_projet
ifdef NOM
	@make check_clean
	@make up
	@sleep 5
	@echo "suppression de la base de données"
	@$(DK) exec db mysql -u$(MYSQL_USER) -p$(MYSQL_PASSWORD) -e "DROP DATABASE $(NOM)"
	@make down
	@echo "suppression du projet $(NOM)"
	@rm -rf $(APP_PATH)/$(NOM)
	@echo "suppression du virtualhost"
	@rm -f  ./virtualhosts/$(NOM).conf
	@make up
else 
	@echo "il faut ajouter le nom du projet à la commande" 
endif

# utilisation de mysqldump pour générer le code sql dans le conteneur db
dump: ## Sauvegarde la base associée au projet : make dump nom_du_projet
ifdef NOM
	@make up
	@$(DK) exec db mysqldump -p$(MYSQL_PASSWORD) $(NOM) > $(NOM)-$(CURRENT_TIME).sql
	@echo "Sauvegarde de la BD du projet $(NOM) réalisée dans le fichier $(NOM)-$(CURRENT_TIME).sql"
else 
	@echo "il faut ajouter le nom du projet à la commande"
endif

dumpInsert: ## Sauvegarde que les données la base associée au projet : make dumpInsert nom_du_projet
ifdef NOM
	@make up
	@$(DK) exec db mysqldump --no-create-info --complete-insert --ignore-table=$(NOM).doctrine_migration_versions --single-transaction -p$(MYSQL_PASSWORD) $(NOM) > $(NOM)-$(CURRENT_TIME)Insert.sql
	@echo "Sauvegarde de la BD du projet $(NOM) réalisée dans le fichier $(NOM)-$(CURRENT_TIME)Insert.sql"
else 
	@echo "il faut ajouter le nom du projet à la commande"
endif

bash: ## Entre en bash dans le conteneur php
	@$(DK) exec php bash

updatePhp: ## Mets à jour composer et le binaire symfony dans le conteneur php
	@make down
	@$(DK) build --no-cache php
	@make up

# permet d'avoir la confirmation de l'utilisateur
check_clean:
	@( read -p "Êtes vous sûr ? Vous allez tout supprimer [o/N]: " sure && case "$$sure" in [oO]) true;; *) false;; esac )

help: ## Affiche cette aide
	@grep --no-filename -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
